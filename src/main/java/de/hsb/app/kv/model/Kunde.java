package de.hsb.app.kv.model;

import java.io.Serializable;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@NamedQuery(name="SelectKunden", query="SELECT k from Kunde k")

@Entity
public class Kunde implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue
	private UUID id;
	private String vorname;
	private String nachname;
	
	@Temporal(TemporalType.DATE)
	private Date geburtsdatum;
	
	public Kunde() {
		this.vorname = "";
		this.nachname = "";
		this.geburtsdatum = null;
	}
	
	public Kunde(String vorname, String nachname, Date geburtsdatum) {
		super();
		this.vorname = vorname;
		this.nachname = nachname;
		this.geburtsdatum = geburtsdatum;
	}


	public String getVorname() {
		return vorname;
	}
	
	public void setVorname(String vorname) {
		this.vorname = vorname;
	}
	
	public String getNachname() {
		return nachname;
	}
	
	public void setNachname(String nachname) {
		this.nachname = nachname;
	}
	
	public Date getGeburtsdatum() {
		return geburtsdatum;
	}
	
	public void setGeburtsdatum(Date geburtsdatum) {
		this.geburtsdatum = geburtsdatum;
	}
	
}
