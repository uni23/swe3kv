package de.hsb.app.kv.controller;

import java.util.GregorianCalendar;
import java.util.List;

import javax.annotation.ManagedBean;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.faces.annotation.ManagedProperty;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.Transactional;
import javax.transaction.UserTransaction;

import de.hsb.app.kv.model.Kunde;
import java.io.Serializable;

@ManagedBean
@Named("kundenHandler")
@SessionScoped
public class KundenHandler implements Serializable {	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@PersistenceContext(name="kv-persistence-unit")
	private EntityManager em;
	
	@Resource
	private UserTransaction utx;
	
	private DataModel<Kunde> kunden;
	
	@ManagedProperty(value="#merkeKunde")
	private Kunde merkeKunde = new Kunde();
	
	@ManagedProperty(value="#storedKunden")
	private List<Kunde> storedKunden;

	
	@PostConstruct
	public void init() {
				
		try {
			utx.begin();
		} catch (NotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		em.persist(new Kunde("Hugo", "Herman", new GregorianCalendar(1999, 3, 15).getTime()));
		
		kunden = new ListDataModel<>();
		kunden.setWrappedData(em.createNamedQuery("SelectKunden").getResultList());
		
		try {
			utx.commit();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (RollbackException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (HeuristicMixedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (HeuristicRollbackException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public DataModel<Kunde> getKunden() {
		return kunden;
	}

	public void setKunden(DataModel<Kunde> kunden) {
		this.kunden = kunden;
	}

	public Kunde getMerkeKunde() {
		return merkeKunde;
	}

	public void setMerkeKunde(Kunde merkeKunde) {
		this.merkeKunde = merkeKunde;
	}

	public List<Kunde> getStoredKunden() {
		return storedKunden;
	}

	public void setStoredKunden(List<Kunde> storedKunden) {
		this.storedKunden = storedKunden;
	}
	
	public String neu() {
		merkeKunde = new Kunde();
		return "neuerKunde";
	}
	
	@Transactional
	public String speichern() {
		merkeKunde = em.merge(merkeKunde);
		em.persist(merkeKunde);
		
		kunden.setWrappedData(em.createNamedQuery("SelectKunden").getResultList());
	
		return "alleKunden";
	}
}
